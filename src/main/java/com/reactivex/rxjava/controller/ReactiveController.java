package com.reactivex.rxjava.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.time.Duration;

@RestController
public class ReactiveController {


    @GetMapping(produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<Integer> randomNumber() {
        return Flux.just(1,2,3,4,5,6)
                .delayElements(Duration.ofSeconds(1));
    }
}
